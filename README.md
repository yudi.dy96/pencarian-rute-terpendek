Algortima pencarian Depth First mencoba untuk mengeksplore atau menjelajahi salah satu 
cabang terlebih dahulu, pada percobaan diatas cabang yang dijelajahi adalah cabang yang paling 
kiri sesuai juga dengan bentuk struktur datanya, pada Gambar 5.3 terlihat bahwa perjalanan dari 
kota Pekalongan Menuju ke Jogja akan melewati rute sebagai berikut :
Pekalongan – Weleri – Semarang – Jogja
Pada hasil percobaan tersebut didapatkan rute dari pekalongan ke jogja mengambil rute yang 
disusun pada cabang paling kanan. Pada percobaan ini belum mengindiasikan untuk menemukan 
rute terpendek ataupun yang paling efektif ketika akan bepergian dari kota Pekalongan ke kota 
Jogja melalui transportasi darat yaitu bus. 

Pada percobaan selanjutnya susunan struktur data dibuat seperti pada gambar 5.2 yang 
diturunkan dari pohon keputusan sesuai Gambar 5.7. Hasil running program sesuai Gambar 
5.6 didapatkan hasil bahwa algoritma pencarian Depth First digunakan untuk menelusuri kota 
– kota yang dilewati antara Kota Pekalongan ke Jogja, sehingga seorang Sales dapat memlih 
perjalanan ini untuk dapat sampai ke Jogja sembari memaksimalkan jumlah kota yang dapat 
disinggahi selama melakukan perjalanan (kasus Traveling Salesman Problem). Pada program 
ini masih mengalami kekuranagn karena proses hanya melakukan pencarian pada dua titik yaitu 
titik asal dan tujuan. Pada program travling yang seharusnya program harus memuat titik – titik 
(node) yang harus dilewati kemudian ditelusuri perjalanan yang paling optimal dilakukan. Akan 
tetapi pad program ini hanya ditentukan sebagaimana orang bepergian dari satu kota ke kota lain 
sebagai tujuan, dan kota yang dilewati yang paling banyaklah yang menjadi acuan untk dapat 
disinggahi.

Hasil yang didapat juga menggambarkan kelemahan dari proses pencarian algoritma Depth 
First yang akan selalu menjelajahi rute mulai dari kiri pohon, ketika susunan pohon keputusan 
dibuat sedemikian rupa sehingga setiap cabangnya mempunyai goal (tujuan) maka rute yang 
ditemukan adalah rute yang disusun pada cabang paling kiri, jika cabang paling kiri tidak 
ditemukan tujuan maka proses pencarian akan dilanjutkan pada cabang berikutnya.
