void setup(void)
{
    assert_flight(“pekalongan”, “weleri”, 50);
    assert_flight(“weleri”, “semarang”, 40);
    assert_flight(“semarang”, “magelang”, 60);
    assert_flight(“magelang”, “jogja”, 50);
    assert_flight(“weleri”, “sukorejo”, 30);
    assert_flight(“sukorejo”, “temanggung”, 50);
    assert_flight(“temanggung”, “magelang”, 60);
    assert_flight(“pekalongan”, “semarang”, 110);
    assert_flight(“semarang”, “jogja”, 110);
}
